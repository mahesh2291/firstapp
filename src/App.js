import React from 'react';

import {BrowserRouter,Route,Link} from "react-router-dom"

import './App.css';

import Login from './components/Login';
import Chromium from './components/Chromium';


function App() {
  return (
    <BrowserRouter>
    <div className="App">
    <Route path="/" exact component={Login} />
    <Route path="/chromium" exact strict component={Chromium}/>

  
    </div>
    </BrowserRouter>
  );
}

export default App;
