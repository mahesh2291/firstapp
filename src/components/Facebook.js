

import React, { Component } from "react";
import FacebookLogin from "react-facebook-login";
import { Redirect } from "react-router-dom";

class Facebook extends Component {
  state = {
    isLoggedIn: false,
  };

  responseFacebook = response => {
    

    this.setState({
      isLoggedIn: true,
     
    });
  };

  componentClicked = () => console.log("clicked");

  render() {
    let fbContent;

    if (this.state.isLoggedIn) {
      return (
          <Redirect to="/chromium" />
      );
    } else {
      fbContent = (
        <FacebookLogin
          appId="2313013165673852"
          autoLoad={true}
          fields="name,email,picture"
          onClick={this.componentClicked}
          callback={this.responseFacebook}
        />
      );
    }

    return <div>{fbContent}</div>;
  }
}

export default Facebook;