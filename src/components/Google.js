import React, { Component } from "react";
import GoogleLogin from "react-google-login";
import { Redirect } from "react-router-dom";


class Google extends Component {
    constructor(props) {
        super(props) 
        this.state={
            isLoggedIn:false
        }
    }
    responseGoogle(response) {
        this.setState({
            isLoggedIn:true
        })
    }
    render() {
        let googlecontent;

        if(this.setState.isLoggedIn) {
            googlecontent=(
                <Redirect to="/chromium" />
            )
        }
        else {
            googlecontent=(
            <GoogleLogin
            clientId="425136859165-fp3unjgfkqrgt1rt7or2akfflncdvmno.apps.googleusercontent.com"
            buttonText="Login"
            onSuccess={this.responseGoogle}
            onFailure={this.responseGoogle}
            cookiePolicy={'single_host_origin'}
          />
            )
        }
return <div>{googlecontent}</div>
        
    }
}

export default Google