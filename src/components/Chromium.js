import React, { Component } from "react"
import docxConverter from "docx-pdf"
import "./mystyles.css"


class Chromium extends Component {

    constructor(props) {
        super(props) 
        this.state={
            imageUpload:false
        }
    }

   upload(e) {
       
       

       this.setState({
           imageUpload:e.target.files
       })
    }

    convert() {
        docxConverter(this.state.imageUpload,'./output.pdf',function(err,result) {
            if(err) {
                console.log(err);
            }
            console.log('result'+result);
        }
        )
       }

    render(){
        
        return (
            <div>
                <h1 className="title">welcome to chromium</h1>
                <input type="file" accept=".docx" onChange={(e)=>this.upload(e)}/>
                <div>
                <button className="button1">Convert</button>
                </div>
            </div>
        )
    }
}

export default Chromium